package ru.urfu.autoassignmentreviewergitlab.configuration;

import org.gitlab4j.api.GitLabApi;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ConfigProperties.class)
public class ProjectConfig {

    @Bean
    public GitLabApi gitLabApi(ConfigProperties configProperties) {
        return new GitLabApi(configProperties.getHost(), configProperties.getAdminToken());
    }
}
