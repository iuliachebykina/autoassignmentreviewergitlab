package ru.urfu.autoassignmentreviewergitlab.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "app")
@Data
public class ConfigProperties {
    private String host;
    private String adminToken;

}
