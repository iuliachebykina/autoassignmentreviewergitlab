package ru.urfu.autoassignmentreviewergitlab.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import ru.urfu.autoassignmentreviewergitlab.dto.RequestBodyDto;
import ru.urfu.autoassignmentreviewergitlab.service.Resolver;

@RestController
public class EventController {

    private final static String HOOK = "Merge Request Hook";
    private final static String X_GITLAB_TOKEN = "x-gitlab-token";
    private final static String X_GITLAB_EVENT = "x-gitlab-event";

    private final Resolver resolver;

    private static final Logger log = LoggerFactory.getLogger(EventController.class);


    @Value("${app.hook_token}")
    private String hookToken;

    public EventController(Resolver resolver) {
        this.resolver = resolver;
    }


    @PostMapping("/webhooks")
    public String postMergeRequest(@RequestHeader MultiValueMap<String, String> headers, @RequestBody RequestBodyDto requestBody){
        if (!headers.get(X_GITLAB_EVENT).get(0).equals(HOOK))
            return "wrong event";
        if (!headers.get(X_GITLAB_TOKEN).get(0).equals(hookToken))
            return "wrong token";

        var projectId = requestBody.getProject().getId();
        var mrIid = requestBody.getObjectAttributes().getIid();
        var action = requestBody.getObjectAttributes().getAction();

        var handler = resolver.resolve(action);
        if(handler == null)
            log.warn("wrong action");
        else
            handler.handle(mrIid, projectId);
        return "ok";
    }
}
