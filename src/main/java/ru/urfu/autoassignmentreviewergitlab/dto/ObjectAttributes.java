package ru.urfu.autoassignmentreviewergitlab.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ObjectAttributes {
    private Integer iid;
    private String action;
}
