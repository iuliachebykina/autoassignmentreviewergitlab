package ru.urfu.autoassignmentreviewergitlab.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class RequestBodyDto {
    private Project project;
    @JsonProperty("object_attributes")
    private ObjectAttributes objectAttributes;
}
