package ru.urfu.autoassignmentreviewergitlab.handler;


public interface MergeRequestActionHandler {
    void handle(Integer mergeRequestIid, Integer projectId);

    String getActionType();
}
