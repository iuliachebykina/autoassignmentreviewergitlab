package ru.urfu.autoassignmentreviewergitlab.handler;


import org.springframework.stereotype.Service;

@Service
public class UpdateMergeRequestHandler implements MergeRequestActionHandler {
    @Override
    public void handle(Integer mergeRequestIid, Integer projectId) {
        // проверка, сменился ли ревьюер в ручную и смена нагрузок, если потребуется
    }

    @Override
    public String getActionType() {
        return "update";
    }
}
