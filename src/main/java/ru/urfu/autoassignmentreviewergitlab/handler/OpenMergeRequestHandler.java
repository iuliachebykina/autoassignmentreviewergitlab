package ru.urfu.autoassignmentreviewergitlab.handler;


import org.springframework.stereotype.Service;

@Service
public class OpenMergeRequestHandler implements MergeRequestActionHandler {
    @Override
    public void handle(Integer mergeRequestIid, Integer projectId) {
        //назначение ревьюера + добавляется нагрузка
    }

    @Override
    public String getActionType() {
        return "open";
    }
}
