package ru.urfu.autoassignmentreviewergitlab.handler;


import org.springframework.stereotype.Service;

@Service
public class CloseMergeRequestHandler implements MergeRequestActionHandler {
    @Override
    public void handle(Integer mergeRequestIid, Integer projectId) {
        // убавление нагрузки у ревьюера
    }

    @Override
    public String getActionType() {
        return "close";
    }
}
