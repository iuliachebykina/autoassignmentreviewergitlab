package ru.urfu.autoassignmentreviewergitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoAssignmentReviewerGitlabApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoAssignmentReviewerGitlabApplication.class, args);
    }

}
