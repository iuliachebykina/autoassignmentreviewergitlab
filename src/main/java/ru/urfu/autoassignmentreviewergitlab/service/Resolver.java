package ru.urfu.autoassignmentreviewergitlab.service;


import org.springframework.stereotype.Service;
import ru.urfu.autoassignmentreviewergitlab.handler.MergeRequestActionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class Resolver {
    private final Map<String, MergeRequestActionHandler> handlers;

    public Resolver(List<MergeRequestActionHandler> handlers) {
        this.handlers = new HashMap<>();
        for (MergeRequestActionHandler handler :
                handlers)
            this.handlers.put(handler.getActionType(), handler);
    }


    public MergeRequestActionHandler resolve(String action){
        return handlers.get(action);
    }
}
